+++
title = "Starting My Blog"
date = 2018-07-23T15:56:19+02:00
draft = false
tags = []
categories = []
+++

## summary
the first post is about setting the blog framework itself

## requirements
- git integrated
- more specifically, *gitlab* integrated
- vi friendly
- .md file friendly
- mac client (linux is also fine; I ignore any kind of vindoze)

## workflow
- create a public project on gitlab
- clone your project
- add content with *vi*, use *markdown* for decoration
- use `git push` to publish
- use a public URL like foo.gitlab.io/bar

## basic setup
- login to *gitlab.com*
  - create a project called *blog*
  - set its visibility to *Public*
  - leave it empty
- get the *hugo* client
  - `brew install hugo`
- create a new site
  - `hugo new site blog`
- connect your local hugo repo with gitlab
  - `cd blog && git init`
  - `git remote add origin git@gitlab.com:langyal/blog.git`
- add a theme to hugo
  - `git submodule add https://github.com/avianto/hugo-kiera.git themes/kiera`
  - if you don't like it, choose a different one: https://themes.gohugo.io/
- set up your *config.toml*

``` baseurl = "https://langyal.gitlab.io/blog"
title = "langyal's blog"
canonifyurls = true
theme = "kiera"

paginate = 3

summaryLength = 30
enableEmoji = true
pygmentsCodeFences = true

[author]
  linkedin = "angyallaszlo"

[params]
    tagline = "snippets related to my work and interest"

[[menu.main]]
    name = "Home"
    weight = 10
    url = "/"
[[menu.main]]
    name = "Posts"
    weight = 20
    url = "/posts/"
```

- add your first post
  - `hugo new posts/starting-my-blog.md`
  - vi content/posts/starting-my-blog.md
  - change `draft = true` to `draft = false`

- launch your server locally and check the result
  - `hugo server`
  - open http://localhost:62527/blog/ (obviously, this can be different in your case)

- the *public* directory
  - if you run just `hugo` instead of `hugo server`, you will find content under the dir. *public*
  - this is not needed for us since we will publish our content differently
  - you can remove this dir. at any time; it will be recreated upon a new run: `rm -rf public; hugo` 
  - anyway, we don't want to publish this, so: `echo public/ >.gitignore`

- add a proper `.gitlab-ci.yml` to your project

``` # All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

- publish your site
  - git add . && git commit -m "initial setup" && git push --set-upstream origin master
  - enjoy the result: open https://langyal.gitlab.io/blog

- following posts
  - `hugo new posts/purpose-of-my-blog.md`
  - `vi content/posts/purpose-of-my-blog.md`
  - change `draft = true` to `draft = false`
  - `git add content/posts/purpose-of-my-blog.md && git commit -m "purpose-of-my-blog.md" && git push`

## sources
https://gohugo.io/getting-started/usage/
https://boats.gitlab.io/blog/post/2017-09-28-blogging-with-gitlab-and-hugo/
https://opensource.com/article/18/3/start-blog-30-minutes-hugo


